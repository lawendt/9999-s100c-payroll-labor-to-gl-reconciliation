

declare @StartDirect integer = 5000
declare @EndDirect integer = 5599
declare @FY integer = 2020

declare @Variance float = 0.05

-- Create temp table for detail results
if object_id('tempdb..#Results') is not null
	drop table #Results

-- Create temp table for details
if object_id('tempdb..#DetailData') is not null
	drop table #DetailData

-- List of all GL records with matching direct costs
select gl.recnum as GLrecnum
	, gl.lgrrec as GLlgrrec
	, gl.srcnum as Source
	, gl.GLDirectTotal
	, isnull(jc.JobCost, 0) as JobCost
	, gl.GLDirectTotal - isnull(jc.JobCost, 0) as GL_JC_Var
	into #Results
	from (
		-- GL Direct Cost Totals by Lgrrec
		select lt.recnum
			, lt.lgrrec
			, max(lt.srcnum) as srcnum
			, sum(dbtamt - crdamt) as GLDirectTotal 
			from lgtnln ll
			join lgrtrn lt on ll.recnum = lt.recnum
			where lt.postyr = @FY and lt.status <> 3
				and ll.lgract between @StartDirect and @EndDirect
			group by lt.recnum, lt.lgrrec
	) gl 
	left join (
		-- Job Cost Totals by Lgrrec
		select jc.lgrrec
			, jc.srcnum
			, sum(jc.cstamt) as JobCost
			from jobcst jc 
			where jc.postyr = @FY and jc.status <> 2
			group by jc.lgrrec, jc.srcnum
		) jc on gl.lgrrec = jc.lgrrec

-- Items with a material variance
select * from #Results 
	where abs(GL_JC_Var) > @variance
	order by GLrecnum


-- Total corrections by job number
select c.jobnum
	, round(sum(c.allocamt),2) as JobCorrection from 
	(
		-- Break down of variance by job number
		select jc.lgrrec  
			, jc.JobNum
			, r.JobCost as TotalJobCost
			, r.GL_JC_Var as TotalGLVariance
			, jc.JobCost
			, jc.JobCost/r.JobCost as AllocPct 
			, r.GL_JC_Var * (jc.JobCost/r.JobCost) as AllocAmt
			from (
				-- Total costs by jobs for GL records with a variance
				select  jc.lgrrec
						, jc.jobnum
						, sum(jc.cstamt) as JobCost
					from jobcst jc 
					join (
						-- Items with a material variance
						select * from #Results 
							where abs(GL_JC_Var) > @variance
						) r on jc.lgrrec = r.gllgrrec
					group by jc.lgrrec, jc.jobnum
					-- order by jc.lgrrec, jc.jobnum
				) jc
			join #Results r on jc.lgrrec = r.GLlgrrec
			-- order by jc.lgrrec, jc.jobnum
		) c
		group by c.jobnum 
		order by c.jobnum