* Calculates general ledger labor cost totals compared to payroll labor costs and looks for variances
* Version 01 TODO LIST
*
* History
*		11/18/10	(law) Original programming

SET safety off 
SET DELETED ON 
CLOSE TABLES
SET CONFIRM ON 
SET TALK OFF 
CLEAR 


PUBLIC datadir, startperiod, endperiod, exportdetail, fileroot


* User paramenters required
datadir = "x:\mb7\uas\"
startperiod = 1
endperiod = 9
fileroot = "UAS 2010 Payroll Labor to GL Reconciliation " + ALLTRIM(STR(startperiod,2,0)) + " to " + ;
	ALLTRIM(STR(endperiod,2,0)) + " - "


* The following paramenters should be extracted from the gl setup (msctbl) information
startdirect = 5000
enddirect = 5599


DO createlaborGL


USE (datadir + "lgrtrn") SHARED IN 0 NOUPDATE
USE (datadir + "lgtnln") SHARED IN 0 NOUPDATE
USE (datadir + "payrec") SHARED IN 0 noupdate

* Get the GL Transaction and Payroll Transaction for qualifying 
SELECT gl.recnum, gl.trnnum, gl.dscrpt, gl.status, gl.srcnum, gl.actprd, ;
	pr.recnum as payrec, pr.status as prstatus, pr.grspay ;
	FROM lgrtrn gl JOIN payrec pr ;
		ON gl.lgrrec = pr.lgrrec ;
	WHERE gl.status <> 3 AND gl.srcnum = 16 ;
	INTO CURSOR GLReport NOFILTER READWRITE 

* Get the ledger transaction lines that apply
SELECT lgtnln.recnum, SUM(dbtamt - crdamt) as GLAmount ;
	FROM lgtnln JOIN GLReport ON lgtnln.recnum = GLReport.recnum ;
	JOIN laborgl ON lgtnln.lgract = laborgl.actnum ;
	INTO CURSOR GLLines NOFILTER READWRITE GROUP BY 1 
	
* Combine the payroll gross pay and gl amount 
SELECT glreport.*, NVL(gllines.glamount,000000000.00) as glamount ;
	FROM glreport ;
	LEFT JOIN gllines ON glreport.recnum = gllines.recnum ;
	INTO CURSOR GLReport NOFILTER READWRITE 

SELECT glreport.*, (glreport.grspay - glreport.glamount) as variance ;
	FROM glreport INTO CURSOR glreport NOFILTER READWRITE 

SELECT glreport.* FROM glreport WHERE ABS(variance) > .05 INTO CURSOR glvariance

SELECT glreport
COPY TO (fileroot + " - Payroll Labor GL Reconciliation") TYPE xl5

SELECT glvariance
COPY TO (fileroot + " - Payroll Labor Variance Only") TYPE xl5

RETURN 

*-----------------------------------------------------------------------------*
PROCEDURE CreateLaborGL
*-----------------------------------------------------------------------------*
* Creates cursor of Labor GL Accounts

CREATE TABLE LaborGL (;
	actnum		n(15,0)) 

SELECT LaborGL

INSERT INTO LaborGL (actnum) VALUES (5400)
INSERT INTO LaborGL (actnum) VALUES (5600)
INSERT INTO LaborGL (actnum) VALUES (5610)
INSERT INTO LaborGL (actnum) VALUES (6322)
INSERT INTO LaborGL (actnum) VALUES (6324)
INSERT INTO LaborGL (actnum) VALUES (6325)
INSERT INTO LaborGL (actnum) VALUES (7400)

RETURN 

