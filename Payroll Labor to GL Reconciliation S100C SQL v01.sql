/* Reconcile job costs to General Ledger

This script compares the gross pay posted to the dedicated payroll account
in the GL to the gross wages in job costing.  if there is a variance, then 
there has been an adjustment in the posting, so the record requires
examination.

	5/28/20 (law) Initial script from legacy VFP version
	11/21/20 (law) Updated logic to compare all 

*/

declare @StartDirect integer = 5000
declare @EndDirect integer = 5599

declare @Variance float = 0.05


-- GL Transaction and payroll transactions for qualifying records
select	gl.recnum
		,gl.trnnum
		,gl.dscrpt
		,gl.status
		,gl.srcnum
		,gl.actprd
		,pr.recnum as payrec
		,pr.status as prstatus
		,pr.grspay as prgrosspay
		,glt.GLAmount
		,pr.grspay - glt.GLAmount as Variance
		,jc.jcgrosspay
		,jc.mininserttime
		,jc.maxinserttime
		,datediff(s, jc.mininserttime, jc.maxinserttime) as TimeDiffSeconds
		,jc.TandMStatus
	from lgrtrn gl 
	join payrec pr on gl.lgrrec = pr.lgrrec
	left join (
		-- GL Transaction lines from the selected set of records
		select	l.recnum
			, sum(l.dbtamt - l.crdamt) as GLAmount
			from lgtnln l 
			join (
				-- GL Transaction and payroll transactions for qualifying records
				select	gl.recnum
					from lgrtrn gl 
					join payrec pr on gl.lgrrec = pr.lgrrec
					where gl.status <> 3 and gl.srcnum = 16
			) g on l.recnum = g.recnum
			join (
				-- Labor GL Accounts from payroll positions
				select distinct jobact as LaborGL from paypst
				union select distinct eqpact as LaborGL from paypst
				union select distinct othact as LaborGL from paypst
			) la on l.lgract = la.LaborGL
			group by l.recnum
	) glt on gl.recnum = glt.recnum
	left join (
		-- gross wages in job cost by payroll record
		select j.payrec
			, max(j.lgrrec) as lgrrec
			, sum(j.grswge) as jcgrosspay
			, min(j.insdte) as mininserttime
			, max(j.insdte) as maxinserttime
			, max(j.bllsts) as TandMStatus
			from jobcst j
			where j.status <> 2 and j.srcnum = 16
			group by j.payrec
		) jc on gl.lgrrec = jc.lgrrec
	where gl.status <> 3 and gl.srcnum = 16
		and abs(pr.grspay - glt.GLAmount) > @Variance
	order by payrec



return 


